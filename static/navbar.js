var scrolling = false;
var prevScrollpos = window.pageYOffset;

hideable = function() {
    scrolling = false;
}

window.onscroll = function() {
    if (!scrolling) {
        checkScroll();
    }
}

window.checkScroll = function() {
    var currentScrollPos = window.pageYOffset;
        if (prevScrollpos >= currentScrollPos + 40) {
            document.getElementById("navbar").style.top = "0";
        }
        else {
            document.getElementById("navbar").style.top = "-300px";
        }
        prevScrollpos = currentScrollPos;
}