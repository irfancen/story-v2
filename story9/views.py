from django.shortcuts import render, redirect
from .forms import CreateUserForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

def index(request):
    context = {}
    return render(request, 'story9/index.html', context)

def registerUser(request):
    form = CreateUserForm()
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Account was succesfully created')
            return redirect('/login')
        else:
            messages.error(request, 'Account creation was unsuccessful')
    context = {
        'form' : form
    }
    return render(request, 'story9/register.html', context)

def loginUser(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/')
        else:
            messages.error(request, 'Username or Password is incorrect')
    context = {}
    return render(request, 'story9/login.html', context)

def logoutUser(request):
    logout(request)
    return redirect('/')