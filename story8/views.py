from django.shortcuts import render
from django.http import JsonResponse
import requests

def index(request):
    return render(request, 'story8/index.html')

def search(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    data = requests.get(url)
    json = data.json()
    return JsonResponse(json)