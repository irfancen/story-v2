from django.test import TestCase, Client

class TestStory8(TestCase):
    def test_url_slash_exist(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)

    def test_search(self):
        response = Client().get('/story8/search/?q=sena')
        content = response.content.decode("utf-8")
        self.assertEqual(response.status_code, 200)
        self.assertIn('kind', content)
        self.assertIn('totalItems', content)
        self.assertIn('items', content)
        self.assertEqual(response['content-type'], 'application/json')