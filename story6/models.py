from django.db import models

class Activities(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class People(models.Model):
    name = models.CharField(max_length=20)
    activities = models.ManyToManyField(Activities)

    def __str__(self):
        return self.name