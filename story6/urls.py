from django.urls import path
from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.index, name='index'),
    path('add/', views.add, name='add'),
    path('addParticipants/<str:pk>/', views.addParticipants, name='addParticipants'),
    path('deleteActivity/<str:pk>/', views.deleteActivity, name='deleteActivity'),
    path('deletePerson/<str:pk>/', views.deletePerson, name='deletePerson'),
]