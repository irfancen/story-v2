# Generated by Django 3.1.2 on 2020-10-23 15:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story6', '0005_auto_20201023_2229'),
    ]

    operations = [
        migrations.AlterField(
            model_name='people',
            name='activities',
            field=models.ManyToManyField(to='story6.Activities'),
        ),
    ]
