from django import forms
from . import models
from crispy_forms.helper import FormHelper

class ActivityForm(forms.ModelForm):
    class Meta:
        model = models.Activities
        fields = '__all__'

    name = forms.CharField(label='Activity Name', max_length=20, widget=forms.TextInput(attrs=
    {
        'type' : 'text',
        'placeholder': 'Activity Name',
        'required' : 'True',
    }))


class PeopleForm(forms.ModelForm):
    class Meta:
        model = models.People
        fields = '__all__'

    name = forms.CharField(label='', max_length=20, widget=forms.TextInput(attrs=
    {
        'type' : 'text',
        'placeholder' : 'Insert Name',
        'required' : 'True',
    }))