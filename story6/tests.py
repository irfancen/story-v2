from django.test import TestCase, Client
from .models import *

class TestStory6(TestCase):
    def setUp(self):
        Activities.objects.create(name="Story 6 PPW")
        Activities.objects.create(name="Lab 2 SDA")
        p1 = People.objects.create(name="Pak Pewe")
        p2 = People.objects.create(name="Sis Dea")
        p1.save()
        p2.save()
        p1.activities.add(Activities.objects.get(name="Story 6 PPW"))
        p2.activities.add(Activities.objects.get(name="Lab 2 SDA"))

    def test_add_activity(self):
        count = Activities.objects.all().count()
        Activities.objects.create(name="Tidur")
        self.assertEqual(Activities.objects.all().count(), count+1)

    def test_add_person(self):
        count = People.objects.all().count()
        p3 = People.objects.create(name="Dek Depe")
        p3.save()
        p3.activities.add(Activities.objects.get(name="Lab 2 SDA"))
        self.assertEqual(People.objects.all().count(), count+1)

    def test_activity_name(self):
        activity = Activities.objects.all()[0]
        self.assertEqual(str(activity), "Story 6 PPW")

    def test_person_name(self):
        person = People.objects.all()[0]
        self.assertEqual(str(person), "Pak Pewe")

    def test_add_activity_from_page(self):
        count = Activities.objects.all().count()
        response = Client().post('/story6/add/', data={'name':'Ngoding'})
        self.assertEqual(Activities.objects.all().count(), count+1)
        self.assertEqual(response.status_code, 302)

    def test_add_participant_to_activity_from_page(self):
        count = People.objects.all().count()
        response = Client().post('/story6/addParticipants/1/', data={'name':'Dek Pewe'})
        self.assertEqual(People.objects.all().count(), count+1)
        self.assertEqual(response.status_code, 302)

    def test_add_participant_to_other_activity_from_page(self):
        countP = People.objects.all().count()
        p = People.objects.get(name="Pak Pewe")
        countA = p.activities.all().count()
        response = Client().post('/story6/addParticipants/2/', data={'name':'Pak Pewe'})
        self.assertEqual(People.objects.all().count(), countP)
        self.assertEqual(p.activities.all().count(), countA+1)
        self.assertEqual(response.status_code, 302)

    def test_delete_activity_from_page(self):
        count = Activities.objects.all().count()
        response = Client().get(f'/story6/deleteActivity/{Activities.objects.all()[0].id}/')
        self.assertEqual(Activities.objects.all().count(), count-1)
        self.assertEqual(response.status_code, 302)

    def test_delete_person_from_page(self):
        count = People.objects.all().count()
        response = Client().get(f'/story6/deletePerson/{People.objects.all()[0].id}/')
        self.assertEqual(People.objects.all().count(), count-1)
        self.assertEqual(response.status_code, 302)

    def test_url_slash_exist(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code, 200)

    def test_url_add_exist(self):
        response = Client().get('/story6/add/')
        self.assertEqual(response.status_code, 200)