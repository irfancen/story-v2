from django.shortcuts import render, redirect
from story6.models import *
from story6.forms import *

def index(request):
    data = Activities.objects.all()
    form = PeopleForm()
    context = {
        "data" : data,
        "form" : form,
    }
    return render(request, 'story6/index.html', context)

def addParticipants(request, pk):
    peopleData = People.objects.all()
    activity = Activities.objects.get(id=pk)
    name = request.POST['name']
    if request.method == 'POST':
        person = People(name=name)
        for i in peopleData:
            if person.name == i.name:
                person = People.objects.get(id=i.id)
                person.activities.add(activity)
                return redirect('/story6')
        person.save()
        person.activities.add(activity)
    return redirect('/story6')
    

def add(request):
    form = ActivityForm(request.POST or None)
    if form.is_valid and request.method == 'POST':
        form.save()
        return redirect('/story6')
    context = {
        "form" : form,
    }
    return render(request, 'story6/add.html', context)

def deleteActivity(request, pk):
    activity = Activities.objects.get(id=pk)
    activity.delete()
    return redirect('/story6')

def deletePerson(request, pk):
    person = People.objects.get(id=pk)
    person.delete()
    return redirect('/story6')