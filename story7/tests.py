from django.test import TestCase, Client

class TestStory7(TestCase):
    def test_url_slash_exist(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)
